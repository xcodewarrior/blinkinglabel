//
//  BlinkingLabel.swift
//  BlinkingLabel
//
//  Created by Emrah Korkmaz on 10/24/19.
//


import UIKit

public class BlinkingLabel : UILabel {
    public func startBlinking() {
        let options: UIViewAnimationOptions =  [ .repeat, .autoreverse]
        UIView.animate(withDuration: 0.25, delay: 0.0, options: options, animations: {
            self.alpha = 0
        }, completion: nil)
    }
 
    public func stopBlinking() {
        alpha = 1
        layer.removeAllAnimations()
    }
    
    
    public func getView() -> UIView {
        
        
        var v = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        v.layer.cornerRadius = 5
        v.backgroundColor = UIColor.red
        return v
        
        
    }
    
}
